import requests as req


class ApiService:
    """
    Klasse um ein API-Service zu repräsentieren. Diese Klasse ist dafür zuständig vom Datenprovider
    OpenUV: https://www.openuv.io/ Daten mittels HTTP-Request abzufragen und weiteren Klassen zur Verfügung zu stellen.
    """

    def __init__(self):
        """
        Standard Konstruktor
        """
        self.__response = None

    @property
    def response(self):
        """
        Standard Getter für response Attribut
        :return: API-Abfrage-Resultat als JSON Objekt
        """
        return self.__response

    def fetch_uv_data(self, coords, dt):
        """
        Methode zum Ausführen eines HTTP-Requests and die OpenUV-API
        :param coords: Längen- und Breitengrad des Ortes als Tupel
        :param dt: Datum als datetime String
        """
        payload = {"lat": coords[0], "lng": coords[1], "dt": dt}

        headers = {'content-type': 'application/json', 'x-access-token': '4a6eb6904b086efa5ad10c710d98b15a'}
        try:
            r = req.get('https://api.openuv.io/api/v1/uv', params=payload, headers=headers)
        except Exception as err:
            pass

        self.__response = r.json()
