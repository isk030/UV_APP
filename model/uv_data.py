class UvData:
    """
    Klasse um ein UV-Daten_Entität darzustellen. Diese Klasse übernimmt die benötigten Informationen der API-Anfragen
    und stellt diese als Objekte zur Verfügung.
    """

    def __init__(self, coords, date):
        """
        Standard Konstruktor, um ein UV-Daten Objekt zu erstellen
        :param coords: Längen- und Breitengrad eines Ortes als Tupel
        :param date: Datum als datetime String
        """
        self.coords = coords
        self.date = date
        self.actual_uv_index = None
        self.max_uv_max = None
        self.safe_time_st_1 = None
        self.safe_time_st_2 = None
        self.safe_time_st_3 = None
        self.safe_time_st_4 = None
        self.safe_time_st_5 = None
        self.safe_time_st_6 = None
        self.sunrise = None

    def map_to_model(self, json_res):
        """
        Methode zur Umwandlung der rohen JSON Antwort der API. Hier werden die benötigten Daten
        mit dem Model verknüpft
        :param json_res: JSON Objekt aus dem API-Call
        """
        result = json_res['result']
        self.actual_uv_index = result['uv']
        self.max_uv_max = result['uv_max']
        self.safe_time_st_1 = result['safe_exposure_time']['st1']
        self.safe_time_st_2 = result['safe_exposure_time']['st2']
        self.safe_time_st_3 = result['safe_exposure_time']['st3']
        self.safe_time_st_4 = result['safe_exposure_time']['st4']
        self.safe_time_st_5 = result['safe_exposure_time']['st5']
        self.safe_time_st_6 = result['safe_exposure_time']['st6']
        self.sunrise = result['sun_info']['sun_times']['sunrise']

    def __str__(self):
        """
        Überschreibung der __str__() Methode zur just-enough anzeige von Objekt-Attributen. Verwendet nur zur Entwicklung
        :return: Ausgabestring des Objekts
        """
        return {'coord':self.coords, 'date':self.date, 'actual_value':self.actual_uv_index, 'max_uv_value':self.max_uv_max, 'safe_st1':self.safe_time_st_1, 'safe_st2':self.safe_time_st_2,
                'safe_st3':self.safe_time_st_3, 'safe_st4':self.safe_time_st_4, 'safe_st5':self.safe_time_st_5, 'safe_st6':self.safe_time_st_6, 'sunrise':self.sunrise}
