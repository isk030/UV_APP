import datetime
import json
import os
import threading
import tkinter as tk
import warnings
import webbrowser
from tkinter.messagebox import showwarning
from tkinter.ttk import Progressbar

import folium
import numpy as np
import pandas as pd
from matplotlib import MatplotlibDeprecationWarning
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from scipy.interpolate import interpolate

from view.gui_controller import GuiController


class Gui(tk.Frame):
    """
    Gui Klasse für die TKinter Application. Sie dient als Wrapper beim Aufbau und der Verwaltung der Tkinter Elemente
    """

    def __init__(self, master=None):
        """
        Üblicher Konstruktor für eine Tkinter App. Es wird vom geerbten tk.Frame ein root Fenter erstellt (master) und
        Attribute initialisiert. Zudem wird eine Funktion zur Erzeugung der visuellen Interaktionselemente ausgeführt
        :param master: Hauptfenster vom Typ tk.Tk()
        """
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()
        self.data = None
        self.progressbar = Progressbar(self.master, orient='horizontal', mode='indeterminate')

    def create_widgets(self):
        """
        Methode zur Erzeugung der einzelnen GUI Elemente. Es wird mit dem simplen Pack Layout-Manager gearbeiet
        """
        self.search_entry_label = tk.Label(self)
        self.search_entry_label["text"] = "Bitte geben Sie eine Adresse oder einen Ort ein"
        self.search_entry_label.pack()

        self.search_entry = tk.Entry(self)
        self.search_entry.pack()

        self.search_btn = tk.Button(self, text="suchen", command=self.search)
        self.search_btn.pack(pady=5)

        self.show_map_btn = tk.Button(self, text="Karte zeigen", command=self.show_map)
        self.show_map_btn.pack(pady=5)

        self.map_wait_lb = tk.Label(self)
        self.map_wait_lb.pack()

        self.place_lb = tk.Label(self)
        self.place_lb.pack(pady=5)

        self.fig = Figure(figsize=(5, 4), dpi=100)
        self.widget = None

    def create_dataframe(self, state_geo):
        country_list = []

        for item in state_geo['features']:
            country_list.append(item['properties']['sovereignt'])

        gc = GuiController()
        ad = GuiController()
        if not os.path.exists('ressources\\coords.json'):
            coords = gc.get_multiple_locations(country_list)
            coord_dict = {'coords': coords}
            with open('ressources\\coords.json', 'w') as outfile:
                json.dump(coord_dict, outfile)

        with open('ressources\\coords.json', 'r') as json_file:
            data = json.load(json_file)
            uv_list = ad.get_multiple_uv_data(data.get('coords'))

        result_dict = {'State': country_list, 'UV_values': uv_list}
        return result_dict

    def show_map(self):
        """
        Methode zur Anzeige der Interaktiven Weltkarte.
        """
        t2 = threading.Thread(target=self.use_browser)
        self.map_wait_lb['text'] = 'Karte wird geladen dies kann etwas länger dauern. Bitte warten!...'
        t2.start()

    def search(self):
        """
        Suchmethode um Eingabestring zu validieren und anhand dieser Geodaten abzufragen (Längen- und Breitengrad)
        """
        if self.search_entry.get() == '' or self.search_entry.get()[0] == ' ':
            showwarning('Keine gültige Eingabe', 'Bitte geben einen gültigen Suchtext ein')
        else:
            t = threading.Thread(target=self.fetch_uv_data)
            t.start()

    def use_browser(self):
        """
        Methode um interaktive Weltkarte (im Browser) zu starten. Aktuell nicht bedeutsam umgesetzt.
        """
        m = folium.Map(location=(52, 70), zoom_start=3, min_zoom=3)
        state_geo = os.path.join('ressources\\', 'custom.geo.json')

        with open(state_geo) as read_file:
            geo_data = json.load(read_file)

        d = self.create_dataframe(geo_data)
        state_data = pd.DataFrame(data=d)

        folium.Choropleth(
            geo_data=state_geo,
            name='choropleth',
            data=state_data,
            columns=['State', 'UV_values'],
            key_on='feature.properties.sovereignt',
            fill_color='OrRd',
            fill_opacity=0.7,
            line_opacity=0.2,
            legend_name='Maximaler UV Index für Heute',
            highlight=True,
            popup=state_data
        ).add_to(m)

        m.save('ressources\\m.html')
        self.map_wait_lb['text'] = ''
        chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
        webbrowser.get(chrome_path).open('C:\\Users\\Izzy\\PycharmProjects\\UV_APP_FINAL\\ressources\\m.html')


    def fetch_uv_data(self):
        """
        Methode zur Orchestrierung der GUI Elemente und ihrer Verhalten mit der Datenabfrage.
        Hier werden bestimmte Elemente zu gewissen Zeitpunkten z.B angezeigt ausgeblendet, die Diagrammpräsentation
        ausgeführt und fehlerhafte Userinteraktionen gehandelt.
        """
        try:
            self.progressbar.pack()
            self.update_progress()

            # Deprecationhinweise von matplotlib werden hier unterdrückt, da keine neue Version verfügbar.
            warnings.filterwarnings('ignore', category=MatplotlibDeprecationWarning)

            # Vollständiges "Aufräumen" des alten Diagrams
            self.fig.axes.clear()
            if self.widget:
                self.widget.destroy()
            self.fig = None

            # Initialisieren der Zeichenfläche und des Diagrams
            self.fig = Figure(figsize=(10, 10), dpi=100)
            canvas = FigureCanvasTkAgg(self.fig, self)
            self.widget = canvas.get_tk_widget()

            gc = GuiController()
            location = gc.get_location(self.search_entry.get())

            if location is None:
                self.stop_progress()
                self.progressbar.pack_forget()
                return

            self.data = gc.get_uv_day_data((location.latitude, location.longitude),
                                           gc.mod_datetime(
                                               str(datetime.datetime.now(datetime.timezone.utc))))
            if self.data is None:
                self.stop_progress()
                self.progressbar.pack_forget()
                return

            # Vorbereiten der erhaltenen Daten für die Diagrammzeichnung
            x = np.array([int(x[3]) for x in self.data.get('timestamps')])
            y = np.array(self.data.get('uv_values'))

            self.stop_progress()

            # Falls möglich: Glättung des Graphen im Diagram mittels Spline-Interpolation
            try:
                x_new = np.linspace(x.min(), x.max(), 50)
                a_BSpline = interpolate.make_interp_spline(x, y)
                y_new = a_BSpline(x_new)
            except ValueError:
                x_new = x
                y_new = y

            # Zeichnen des Graphen
            try:
                self.fig.add_subplot().plot(x_new, y_new)
                self.fig.add_subplot().grid()
                self.fig.add_subplot().set_xlabel('Tagesstunde angegeben als Dezimalzahl (24h und UTC)')
                self.fig.add_subplot().set_ylabel('UV Index')
                self.fig.add_subplot().set_title('UV Index Tagesverlauf für den heutigen Tag')

                self.widget.pack()
                self.place_lb['text'] = location.address
            except ValueError:
                showwarning('Nicht genügend Daten',
                            'Es stehen noch keine ausreichenden Daten zur Verfügung. Bitte wählen Sie diesen Standort zu einem anderen Zeitpunkt')
                self.stop_progress()
                self.progressbar.pack_forget()
                return
        except AttributeError:
            pass

    def update_progress(self):
        """
        Simple Methode zur Anzeige und Animation des Ladebalkens
        """
        self.progressbar.start(10)
        self.master.update()

    def stop_progress(self):
        """
        Simple Methode zum Anhalten der Ladebalkenanimation
        :return:
        """
        self.progressbar.stop()
        self.master.update()
