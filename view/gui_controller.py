import datetime
import multiprocessing
import random
from tkinter.messagebox import showwarning

from geopy import Nominatim

from data.api_service import ApiService
from model.uv_data import UvData


class GuiController:

    def __init__(self):
        """
        Blanko Konstruktor, da keine Attribute benötigt werden.
        """
        pass

    def get_uv_data(self, coords, date):
        """
        Methode zum Erhalt eines UV-DatenObjekts vom API-Service
        :param coords: Längen- und Breitengrad als Tupel
        :param date: Datum als datetime String
        :return: UV-Daten Objekt als UVData
        """
        api = ApiService()
        api.fetch_uv_data(coords, date)
        uv_model = UvData(coords, date)
        try:
            uv_model.map_to_model(api.response)
        except:
            showwarning('Fehler bei Datenabruf', 'Bitte probieren Sie es nochmal')
        return uv_model

    def get_location(self, query):
        """
        Methode zur Ermittlung des Längen- und Breitengrades mittels geolocator.
        :param query: Sucheingabe als String
        :return: Ort als Location Objekt
        """
        proxies = [
            {"http": "176.9.75.42:3128"},
            {"http": "88.198.24.108:8080"},
            {"http": "46.4.96.137:3128"},
            {"http": "46.4.96.137:8080"},
            {"http": "88.198.24.108:3128"},
            {"http": "176.9.119.170:3128"},
            {"http": "176.9.75.42:8080"},
            {"http": "88.198.50.103:8080"},
            {"http": "119.81.189.194:8123"},
            {"http": "169.57.1.84:80"},
            {"http": "88.198.24.108:8080"},

        ]
        proxy = random.choice(proxies)
        try:
            geolocator = Nominatim(user_agent='application',
                                   proxies=proxy)
            location = geolocator.geocode(query)
            return location
        except Exception as err:
            showwarning('Ort nicht gefunden', 'Bitte spezifizieren Sie Ihren Sucheintrag')
            return

    def get_uv_day_data(self, coords, date):
        """
        Methode um einzelne UV-Daten Objekte zu Erhalten, um einen Tagesverlauf zu zeichnen
        :param coords: Längen- und Breitengrad als Tupel
        :param date: Datum als datetime String
        :return: Map mit Zeitstempel und UV Index Werten als Dictionary
        """
        try:
            timestamps = []
            uv_values = []
            uv_obj_now = self.get_uv_data(coords, date)
            sunrise_time = uv_obj_now.sunrise
            if sunrise_time is None:
                showwarning('Kein Sonnenaufgang verfügbar',
                            'An diesem Standort ist die Sonne nicht immer sichtbar. Bitte zu einem späteren Zeitpunkt wählen')
                return
            uv_value = uv_obj_now.actual_uv_index
            sunrise_time = self.datesstring_to_datelist(sunrise_time)
            passeddate = self.datesstring_to_datelist(date)
            daterage = int(passeddate[3]) - int(sunrise_time[3]) - 1
            timestamps.append(sunrise_time)

            for i in range(1, daterage + 1):
                daytimes = [int(sunrise_time[0]), int(sunrise_time[1]), int(sunrise_time[2]), int(sunrise_time[3]) + i,
                            int(sunrise_time[4])]
                timestamps.append(daytimes)

            for i in range(0, daterage + 1):
                g_uv_model = self.get_uv_data(coords, str(
                    datetime.datetime(int(timestamps[i][0]), int(timestamps[i][1]), int(timestamps[i][2]),
                                      int(timestamps[i][3]), int(timestamps[i][4]))))
                uv_values.append(g_uv_model.actual_uv_index)

            uv_values.append(uv_value)
            timestamps.append(passeddate)

            return {'timestamps': timestamps,
                    'uv_values': uv_values}
        except AttributeError:
            pass

    def datesstring_to_datelist(self, string):
        """
        Methode um ein datetime Objekt einzelne Strings zu teilen, zur besseren Verwertung des Datumstrings
        :param string: Datum im datetime format als String
        :return: Liste in Form: [jahr, monat, tag, stunde, minute] als Array
        """
        datelist = [string[:4], string[5:7], string[8:10], string[11:13], string[14:16]]
        return datelist

    def mod_datetime(self, time):
        """
        Methode zur einfacheren Zusammensetzung eines Datumstrings, damit datetime Objekt Format und API-Datum-String
        Format übereinstimmen
        :param time: Datumstring als String
        :return: API-Service kompatibler Datumstring
        """
        return time[:10] + 'T' + time[11:23] + 'Z'

    def get_multiple_locations(self, countries):
        '''
        Methode zur parallelen Abfrage der Geo Koordinanten jedes Landes
        :param countries: Liste von Ländernamen als Array mit Strings
        :return: Geo Koordinaten als Array aus Tupeln
        '''
        p = multiprocessing.Pool(25)
        result = p.map_async(self.multi_controller_geo, countries)
        return result.get()

    def multi_controller_geo(self, country):
        '''
        Methode welche parallel ausgeführt wird, um die Geokoordinanten eines Ortes zu ermitteln
        :param country: Ländername als String
        :return: Geo Koordinaten als Tupel
        '''
        gc = GuiController()
        location = gc.get_location(country)
        if location:
            return (location.latitude, location.longitude)
        else:
            return None

    def get_multiple_uv_data(self, coords):
        '''
        Methode zur parallelen Abfrage von UV-Werten.
        :param coords: Liste von Geo Koordinaten als Array aus Tupeln
        :return: Liste von UV-Werten als Array
        '''
        p = multiprocessing.Pool(25)
        result = p.map_async(self.multi_controller_uv, coords)
        result.wait()
        return result.get()

    def multi_controller_uv(self, coord):
        '''
        Methode welche parallel ausgeführt wird, um den max. UV-Wert eines Ortes zu ermitteln
        :param coord: Geo Koordinaten als Tupel
        :return: Geo Koordinaten als Tupel
        '''
        time = str(datetime.datetime.now(datetime.timezone.utc))
        gc = GuiController()
        try:
            uv_data = gc.get_uv_data(coord, time)
            return uv_data.max_uv_max
        except:
            return None
