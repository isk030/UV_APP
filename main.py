"""
Application Hauptzugangspunkt: Diese Applikation bietet die Möglichkeit UV Werte für einen beliebigen Ort darzustellen
für den heutigen Tag. Zudem werden auf einer Weltkarte die maximalen UV-Werte für den akutuellen Tag dargestellt.
"""
import tkinter as tk

from view.gui import Gui

if __name__ == '__main__':
    root = tk.Tk()
    root.geometry('500x600')
    root.title('UV Index App')
    app = Gui(master=root)
    app.mainloop()
